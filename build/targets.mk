# this makefile fragment defines various standard targets
# ls_sources : lists all source files that will be used for compilation
# ls_objects : lists all object files that will be produced
# ls_tests   : lists all tests that will be run
# ls_examples : lists the names of examples that are created
# ls_binaries : lists the names of the binaries that are created
# ls_modules  : lists the modules that will be build

.PHONY : ls_sources ls_objects ls_tests ls_examples ls_binaries ls_modules cppcheck

ls_sources:
	@echo $(SOURCE_FILES)

ls_objects:
	@echo $(OBJECT_FILES)

ls_tests:
	@echo $(TESTS)

ls_examples:
	@echo $(EXAMPLES)

ls_binaries:
	@echo $(BINARIES)

ls_modules:
	@echo $(MAKEFILE_MODULES) $(SRC_MODULES)

# these targets generate products
# tests:  makes tests
# sure :  runs tests
# examples : make examples
# binaries : make binaries
# modules : make all modules
# libs : make the libararies (usually just 1)
# all : makes all products and runs tests
.PHONY: tests binaries examples libs modules sure all clean

tests : $(TESTS) $(BINARIES)

sure : $(addsuffix .passed,$(TESTS)) tests modules
	@export FAILED_TESTS="$(filter $(addsuffix .failed,$(UNIT_TESTS)), $(shell find . -name \*.failed))" && \
        if [ "$${FAILED_TESTS}" = "" ]; then \
		echo All tests passed;\
         else\
	  echo Tests failed: $${FAILED_TESTS};\
          false; \
         fi;

libs: $(BASE_LIB) $(TEST_LIB)

examples : $(EXAMPLES) 

binaries : $(BINARIES)

modules : $(MAKEFILE_MODULES)

cppcheck : $(addprefix $(OBJECT_DIR)/cppcheck/, $(filter lib-src/%.cpp,$(SOURCE_FILES)))

clean : 
	rm -f $(addsuffix .passed,$(TESTS))
	rm -rf $(BASE_DIR)/lib 
	rm -rf $(BASE_DIR)/obj
	rm -rf $(BASE_DIR)/examples
	rm -rf $(BASE_DIR)/bin
	rm -rf $(BASE_DIR)/tests
	rm -rf $(BASE_DIR)/docs/html
	$(foreach mod,$(MAKEFILE_MODULES),$(MAKE) -C $(dir $(mod)) clean;)

# using invoking opt, the compilations are optimized
opt : OPTIMIZE=true
opt : all

coverage : COVERAGE=true
coverage : all 
	lcov -b $(BASE_DIR) -d $(BASE_DIR)/obj -o  $(COVERAGE_INFO) --capture;
	genhtml -o $@ -t "$(BASE_NAME) Coverage" $(COVERAGE_INFO);

all : modules binaries examples libs sure

define GEN_MODULE_RULE =
$(info Generating rule $(1)) 
$(1): $(dir $(1))/Makefile $$(BASE_LIB) $$(shell find "$$(dir $(1))/module-src" -type f)
	@$$(MAKE) -f $$(<) -C $(dir $(1)) all;
endef

$(foreach M,$(MODULES),$(eval $(call GEN_MODULE_RULE,$(M))))
